function getCustomers(){
var mydata;
var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
//alert(url);
var filtro =  document.getElementById('filtro').value;
if(filtro!=''){
  url = url + filtro;
}
var request = new XMLHttpRequest();
request.onreadystatechange = function(){
  if (this.readyState == 4 && this.status == 200) {
    //console.log(request.responseText);
    mydata = request.responseText;
    procesaClientes(mydata);
      //document.getElementById('texto').innerHTML = mydata;
    }
  }

request.open("GET", url, true);
//request.setRequestHeader("Content-type", "application/json");
request.send();
}
function procesaClientes(productosObtenidos) {
  //alert(productosObtenidos);
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
  $("#divTabla").empty();
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    var columnaPrecio = document.createElement("td");
    var columnaStock = document.createElement("td");
    var columnaCity = document.createElement("td");
    var columnaImagen = document.createElement("td");
    var imagen = document.createElement("IMG");
    imagen.classList.add("flag");

    columnaNombre.innerText = JSONProductos.value[i].CustomerID;
    columnaPrecio.innerText = JSONProductos.value[i].CompanyName;
    columnaStock.innerText  = JSONProductos.value[i].Country;
    columnaCity.innerText   = JSONProductos.value[i].City;
  if(JSONProductos.value[i].Country=='Germany'){
    imagen.src=   'img/Alemania.png';
  }else if (JSONProductos.value[i].Country=='Mexico') {
    imagen.src=   'img/Mexico.png';
  }else if (JSONProductos.value[i].Country=='UK') {
    imagen.src=   'img/UK.png';
  }else  { imagen.src=   'img/Ame.png';  }

columnaImagen   = imagen;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);
    nuevaFila.appendChild(columnaCity);
nuevaFila.appendChild(columnaImagen);
    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
