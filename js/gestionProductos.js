function getProductos(){
var mydata;
var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
//alert(url);
var filtro =  document.getElementById('filtro').value;
if(filtro!=''){
  url = url + filtro;
}
var request = new XMLHttpRequest();
request.onreadystatechange = function(){
  if (this.readyState == 4 && this.status == 200) {
    //console.log(request.responseText);
    mydata = request.responseText;
// document.getElementById('texto').innerHTML = mydata;
    procesaProductos(mydata);
    //
    }
  }

request.open("GET", url, true);
//request.setRequestHeader("Content-type", "application/json");
request.send();
}
/*
function procesaProductos(productosObtenidos){
  var JSONProductos = JSON.parse(productosObtenidos);
  document.getElementById('texto').innerHTML = JSONProductos.value[0].ProductName;

}
*/

function procesaProductos(productosObtenidos) {
  //alert(productosObtenidos);
  var JSONProductos = JSON.parse(productosObtenidos);
  //alert(JSONProductos.value[0].ProductName);
    $("#divTabla").empty();
  var divTabla = document.getElementById("divTabla");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    //console.log(JSONProductos.value[i].ProductName);
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    var columnaPrecio = document.createElement("td");
    var columnaStock = document.createElement("td");

    columnaNombre.innerText = JSONProductos.value[i].ProductName;
    columnaPrecio.innerText = JSONProductos.value[i].UnitPrice;
    columnaStock.innerText = JSONProductos.value[i].UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }

  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
